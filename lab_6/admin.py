from django.contrib import admin
from .models import PersonalStatus, Subscriber

# Register your models here.
admin.site.register(PersonalStatus)
admin.site.register(Subscriber)
