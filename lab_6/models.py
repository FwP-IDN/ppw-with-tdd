from django.db import models

# Create your models here.


class PersonalStatus(models.Model):
    created_at = models.DateTimeField()
    status = models.CharField(max_length=100)


class Subscriber(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=300)
