$(document).ready(function(){

    fav_book = new Set([]);
    add_fav = 0;

    function sleep (time) {
        return new Promise((resolve) => setTimeout(resolve, time));
    }

    sleep(1000).then(() => {
        $("#themeChanger").click();
        function gettingBook(query) {
            if(query == "F") {
                alert("Thank you for your respect");
            }
            $.ajax({
                url: "/get_book_api/?q=" + query,
                success: function(result){
                    var booktable = `
                    <script>
                    function changeStar(starId) {
                        if(fav_book.has(starId)) {
                            fav_book.delete(starId);
                            add_fav--;
                            $('#' + starId).css("color", "white");
                        }
                        else {
                            fav_book.add(starId);
                            add_fav++;
                            $('#' + starId).css("color", "orange");
                        }
                        $('#fav_changer').val(add_fav);
                    }
                    </script>
                    `;
                    localStorage.fav_num = 0;
                    if(localStorage.getItem("book") == "undefined" || localStorage.getItem("book") == null) {
                        localStorage.setItem("book", "{}");
                    }
                    booktable += ''
                    if(result.items.length > 0) {
                        booktable += ` 
                        <table class="table table-hover">
                            <thead>
                              <tr bgcolor="#007bff" class="headrow">
                                <th scope="col" style="width: 23%; text-align: center"><font color="white">Judul</font></th>
                                <th scope="col" style="width: 23%; text-align: center"><font color="white">Penulis</font></th>
                                <th scope="col" style="width: 23%; text-align: center"><font color="white">Penerbit</font></th>
                                <th scope="col" style="width: 23%; text-align: center"><font color="white">Gambar</font></th>
                                <th scope="col" style="width: 8%; text-align: center"><font color="white"></font></th>
                              </tr>
                            </thead>
                        `;
                    }
                    else {
                        booktable += '<p class="normal-text"> Wah saya sudah membaca semua buku</p>'
                    }
                    for(var i = 0; i < result.items.length; i++) {
                        var item = result.items[i];
                        if((i % 2) == 0) {
                            booktable += `<tr class="table-primary">`;
                        }
                        else {
                            booktable += `<tr class="table-secondary">`;
                        }
                        var color;
                        console.log(JSON.parse(localStorage.getItem("book"))[item.id]);
                        if(JSON.parse(localStorage.getItem("book"))["star-" + item.id] == 1) {
                            color = "orange";
                            localStorage.fav_num++;
                        }
                        else {
                            color = "white";
                            newBookDict = JSON.parse(localStorage.getItem("book"));
                            newBookDict["star-" + item.id] = 0;
                            localStorage.setItem("book", JSON.stringify(newBookDict));
                        }
                        booktable += `<td class="entry" style="width: 23%; text-align: center">${item.volumeInfo.title}</td>\n`
                        if(item.volumeInfo.authors != undefined) {
                            booktable += `<td class="entry" style="width: 23%; text-align: center">${item.volumeInfo.authors.join()}</td>\n`
                        }
                        else {
                            booktable += `<td class="entry" style="width: 23%; text-align: center">Anonymous</td>\n`
                        }
                        booktable += `<td class="entry" style="width: 23%; text-align: center">${item.volumeInfo.publisher}</td>\n`
                        if(item.volumeInfo.imageLinks !== undefined) {
                            booktable += `<td class="entry" style="width: 23%; text-align: center"><img src="${item.volumeInfo.imageLinks.thumbnail}"></td>\n`
                        }
                        else {
                            booktable += `<td class="entry" style="width: 23%; text-align: center"><img width=128 src="https://images.emojiterra.com/mozilla/512px/2753.png"></td>`
                        }
                        booktable += `<td class="entry" style="width: 8%; text-align: center"><span id="star-${item.id}" class="fa fa-star" onclick="changeStar('star-${item.id}')" style="color: ${color}"></span></td>\n`
                        booktable += `</tr>`
                    }
                    booktable += `    </table>`;
                    $('#booktable').text('');
                    $('#booktable').append(booktable);
                    setTheme();
                }
            });
        }



        function listingSubscribers() {
            $.ajax({
                url: "/list_subscriber_api/",
                success: function(result){
                    var subscriberstable = `
                    <script>
                        $(".btn-unsubscribe").mouseenter(function(e) {
                            $("#hiddenid").val(e.target.id);
                            $("#unsubscribeheader").html("Apakah akun dengan email " + $("#email-" + e.target.id).html() + 
                            " yakin akan unsubscribe dan menghapus akun anda?");
                        });
                    </script>
                    `;
                    if(result.length > 0) {
                        subscriberstable += ` 
                        <table class="table table-hover">
                            <thead>
                              <tr bgcolor="#007bff" class="headrow">
                                <th scope="col" style="width: 10%; text-align: center"><font color="white">No</font></th>
                                <th scope="col" style="width: 40%; text-align: center"><font color="white">Nama</font></th>
                                <th scope="col" style="width: 40%; text-align: center"><font color="white">Email</font></th>
                                <th scope="col" style="width: 10%; text-align: center"><font color="white">Aksi</font></th>
                              </tr>
                            </thead>
                        `;
                    }
                    else {
                        subscriberstable += '<p class="normal-text">Sadly, tidak ada subscriber</p>'
                    }
                    for(var i = 0; i < result.length; i++) {
                        var item = result[i];
                        if((i % 2) == 0) {
                            subscriberstable += `<tr class="table-primary">`;
                        }
                        else {
                            subscriberstable += `<tr class="table-secondary">`;
                        }
                        subscriberstable += `<td class="entry" style="width: 10%; text-align: center">${i+1}</td>\n`
                        subscriberstable += `<td class="entry" style="width: 40%; text-align: center">${item.name}</td>\n`
                        subscriberstable += `<td class="entry" style="width: 40%; text-align: center" id="email-${item.id}">${item.email}</td>\n`
                        subscriberstable += `<td class="entry" style="width: 10%; text-align: center"><button id="${item.id}" class="btn btn-unsubscribe" data-toggle="modal" data-target="#unsubscribeModal">Unsubscribe</button></td>\n`
                        
                        subscriberstable += `</tr>`
                    }
                    subscriberstable += `    </table>`;
                    $('#subscriberstable').text('')
                    $('#subscriberstable').append(subscriberstable);
                    setTheme();
                }
            });
        }

        function getSubscriber() {
            $.ajax({
                url: "/count_subscriber/",
                success: function(result){
                    document.getElementById("subscriber_label").innerHTML = "Subscriber Count: " + result.count;
                },
            });
        }

        $("#registerAsSubscriber").submit(function(e) {
            var frm = $(this)
            e.preventDefault();
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (result) {
                    alert(result.message);
                    handleValidateForm();
                    getSubscriber();
                },
            });
        });

        $("#unsubscribeForm").submit(function(e) {
            var frm = $(this)
            e.preventDefault();
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (result) {
                    alert(result.status);
                    listingSubscribers();
                    $("#close_modal").click();
                    getSubscriber();
                },
            });
        });



        function handleValidateForm() {
            console.log("handleValidateForm");
            var re = /\S+@\S+\S+/;
            $("#register_as_subscriber").removeAttr("disabled");
            var valid = true;
            if($("#inputName1").val() == "") {
                $("#inputName1").tooltip({ items: "#inputName1", content: "Apakah anda tidak memiliki nama?"});
                $("#inputName1").tooltip("open");
                $("#register_as_subscriber").attr("disabled", "disabled");
            }
            else {
                $("#inputName1").tooltip({ items: "#inputName1", content: "Masukkan nama anda. Nam lengkap lebih baik."});
                $("#inputName1").tooltip("close");
            }
            if($("#inputPassword1").val() == "") {
                $("#inputPassword1").tooltip({ items: "#inputPassword1", content: "Apakah anda tidak memberikan proteksi ke akun anda?"});
                $("#inputPassword1").tooltip("open");
                $("#register_as_subscriber").attr("disabled", "disabled");
            }
            else {
                $("#inputPassword1").tooltip({ items: "#inputPassword1", content: "Masukkan password"});
                $("#inputPassword1").tooltip("close");
            }
            $.ajax({
                url: "/check_email_api/?email=" + $("#inputEmail1").val(),
                success: function(result){
                    if(result.result == "REGISTERED" || re.test($("#inputEmail1").val()) == false) {
                        $("#inputEmail1").tooltip({ items: "#inputEmail1", content: "Email ini sudah subscribe ke laman ini atau tidak valid"});
                        $("#inputEmail1").tooltip("open");
                        $("#register_as_subscriber").attr("disabled", "disabled");
                    }
                    else {
                        $("#inputEmail1").tooltip({ items: "#inputEmail1", content: "Masukkan email yang sebelumnya belum pernah dipakai untuk subscribe laman ini"});
                        $("#inputEmail1").tooltip("close");
                    }
                },
            });
        }
        $("#inputName1").keyup(function() {
            handleValidateForm();
        });
        $("#inputEmail1").keyup(function() {
            handleValidateForm();
        });
        $("#inputPassword1").keyup(function() {
            handleValidateForm();
        });
        $('.main_element').css('display', 'block');
        $('.loader').css('display', 'none');
        $("#accordion").accordion();
        $("#accordion").click(function() {
            $(this).accordion();
            setTheme();
        });
        $("#book_trigger").click(function() {
            gettingBook($("#book_name").val());
        });
        $("#book_name").keypress(function(e) {
            if(e.which == 13) {
                gettingBook($("#book_name").val());
            }
        });
        function setTheme() {
            if(localStorage.theme === "light") {
                $("body").css("background-color", "white");
                $("h1").css("color", "black");
                $("h2").css("color", "black");
                $(".navbar").css("background-color", "#007bff");
                $(".btn").css("background-color", "#007bff");
                $(".btn").css("color", "white");
                $("legend").css("color", "black");
                $("label").css("color", "black");
                $(".form-control").css("background-color", "white");
                $(".jumbotron").css("background-color", "#e9ecef");
                $(".headrow").css("background-color", "#007bff");
                $("td").css("color", "black");
                $(".table-secondary").children("td").css("background-color", "#b8daff");
                $(".table-primary").children("td").css("background-color", "#d6d8db");
                $("p").css("color", "black");
                $("#fwp-pic").attr("src", "https://media.licdn.com/dms/image/C5603AQFQAfVMchgMvw/profile-displayphoto-shrink_200_200/0?e=1544659200&v=beta&t=buKPdQjY0lvXuOo3O6IYyie6wzMP9OHZge67GkSNjYg");
                $("#fwp-pic").css("border-color", "#007bff");
                $(".ui-state-default").css("background-color", "#e9ecef");
                $(".ui-state-active").css("background-color", "#007bff");
                $(".ui-widget-content").css("background-color", "white");
                $(".ui-widget-content").css("color", "black");
                $("#introduction").text("Nama saya Febriananda Wida Pramudita, seorang mahasiswa Fasilkom UI angkatan 2017");
                $(".normal-text").css("color", "black");
                $("input").css("color", "black");
            }
            else {
                $("body").css("background-color", "black");
                $("h1").css("color", "white");
                $("h2").css("color", "white");
                $(".navbar").css("background-color", "#003b7f");
                $(".btn").css("background-color", "#003b7f");
                $(".btn").css("color", "white");
                $("legend").css("color", "white");
                $("label").css("color", "white");
                $(".form-control").css("background-color", "black");
                $(".jumbotron").css("background-color", "#161310");
                $(".headrow").css("background-color", "#003b7f");
                $("td").css("color", "white");
                $(".table-secondary").children("td").css("background-color", "#5c6d7f");
                $(".table-primary").children("td").css("background-color", "#6b6c6d");
                $("p").css("color", "white");
                $("#fwp-pic").attr("src", "https://i.imgur.com/nQ2vvcR.jpg");
                $("#fwp-pic").css("border-color", "#003b7f");
                $(".ui-state-default").css("background-color", "#6b6c6d");
                $(".ui-state-active").css("background-color", "#003b7f");
                $(".ui-widget-content").css("background-color", "black");
                $(".ui-widget-content").css("color", "white");
                $("#introduction").text("Nama saya Febriananda Wida Pramudita, saya bukan Batman");
                $(".normal-text").css("color", "white");
                $("input").css("color", "white");
            }
        }

        if (typeof(localStorage) === "undefined") {
            alert("No localStorage, install it or update to html5");
        }

        if(localStorage.theme !== "dark" && localStorage.theme !== "light") {
            localStorage.theme = "light";
        }

        setTheme();

        $("#themeChanger").click(function() {
            if(localStorage.theme === "dark") {
                localStorage.theme = "light";
            }
            else {
                localStorage.theme = "dark";
            }
            setTheme();
        });
        if(window.location.pathname == "/book/") {
            gettingBook("quilting");
        }
        if(window.location.pathname == "/subscribe/") {
            $("#register_as_subscriber").attr("disabled", "disabled");
        }
        getSubscriber();
        if(window.location.pathname == "/subscribers/") {
            listingSubscribers();
        }
    });
})