from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, profile
from .models import Subscriber
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import json
import os
import datetime
import time

# Create your tests here.


class Lab6UnitTestIndex(TestCase):
    def test_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_is_not_exist(self):
        response = Client().get('/random_meong_meong_meong_mbek/')
        self.assertEqual(response.status_code, 404)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_greeting_message(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('apa kabar?', html_response)

    def test_intial_status_is_empty(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<p style="text-align: center">No Schedule Found</p>', html_response)

    def test_add_status(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST = {'status': 'Mengeong'}
        # expected_time = datetime.datetime.now() + datetime.timedelta(hours=7)
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn(
            '<td class="status-data" style="width: 80%; text-align: center">Mengeong</td>', html_response)
        # PREFIX = '<td class="time-data" style="width: 20%; text-align: center">'
        # SUFFIX = '</td>'
        # startIndex = html_response.find(PREFIX) + len(PREFIX)
        # endIndex = html_response.find(SUFFIX, startIndex)
        # recv_time = datetime.datetime.strptime(html_response[startIndex:endIndex]
        #                                        .replace(' p.m.', 'PM')
        #                                        .replace(' a.m.', 'AM')
        #                                        .replace('midnight', '0:00AM'), '%b. %d, %Y, %H:%M%p')
        # # due to python bugs, I do it manually. see this https://bugs.python.org/issue26321
        # recv_time = recv_time + datetime.timedelta(hours=12) * ('p.m' in html_response[startIndex:endIndex])
        # self.assertLess(abs(expected_time - recv_time), datetime.timedelta(seconds=65))

    def test_book(self):
        response = self.client.get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_open_subscribe(self):
        response = self.client.get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_add_fav_book(self):
        resp = self.client.post('/book/', data={'fav_changer': '1'})
        self.assertEqual(resp.status_code, 200)


class Lab6UnitTestProfile(TestCase):
    def test_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_myname_exist(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Febriananda Wida Pramudita', html_response)

    def test_photo_included(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn(
            'https://media.licdn.com/dms/image/C5603AQFQAfVMchgMvw/profile-displayphoto-shrink_200_' +
            '200/0?e=1544659200&v=beta&t=buKPdQjY0lvXuOo3O6IYyie6wzMP9OHZge67GkSNjYg', html_response)

    def test_get_book_api(self):
        response = Client().get('/get_book_api/?q=quilting')
        self.assertEqual(response.status_code, 200)


class Lab6UnitTestSubscriber(TestCase):
    def setUp(self):
        self.subscriber1 = Subscriber(name="Febriananda Wida Pramudita",
                                      email="p.phebri@yahoo.com",
                                      password="aku ganteng parah")
        self.subscriber1.save()

    def test_success_register(self):
        response = self.client.post('/register_as_subscriber/',
                                    data={
                                        "inputName1": "Febriananda Wida Pramudita",
                                        "inputEmail1": "febriwpramu@gmail.com",
                                        "inputPassword1": "aku ganteng parah"})
        self.assertEqual(json.loads(response.content)["message"], "REGISTRASI BERHASIL")

    def test_fail_register(self):
        response = self.client.post('/register_as_subscriber/',
                                    data={
                                        "inputName1": "Mpeb",
                                        "inputEmail1": "p.phebri@yahoo.com",
                                        "inputPassword1": "meong meong"})
        self.assertEqual(json.loads(response.content)["message"], "REGISTRASI GAGAL")

    def test_check_email_api(self):
        response = self.client.get('/check_email_api/?email=p.phebri@yahoo.com')
        self.assertEqual(json.loads(response.content)["result"], "REGISTERED")
        response = self.client.get('/check_email_api/?email=febriwpramu@gmail.com')
        self.assertEqual(json.loads(response.content)["result"], "UNREGISTERED")

    def test_count_subscriber(self):
        subscriber_count = json.loads(self.client.get('/count_subscriber/').content)["count"]
        self.assertEqual(subscriber_count, 1)
        subscribers = json.loads(self.client.get('/list_subscriber_api/').content)
        self.assertEqual(len(subscribers), 1)

    def test_open_page_subscribe(self):
        response = self.client.get('/subscribers/')
        self.assertEqual(response.status_code, 200)

    def test_success_unsubscribe(self):
        response = self.client.post('/unsubscribe/', data={
            'hiddenid': 1,
            'inputPasswordUnsubscribe': 'aku ganteng parah',
        })
        self.assertEqual(json.loads(response.content)["status"], "SUCCESS")

    def test_fail_unsubscribe(self):
        response = self.client.post('/unsubscribe/', data={
            'hiddenid': 1,
            'inputPasswordUnsubscribe': 'meong',
        })
        self.assertEqual(json.loads(response.content)["status"], "FAIL, UNMATCHED PASSWORD")


class E2ETest(TestCase):
    def setUp(self):
        chrome_options = Options()
        self.DEVELOPMENT_ENV = os.environ.get('DEVELOPMENT_ENV')
        assert self.DEVELOPMENT_ENV == 'local' or self.DEVELOPMENT_ENV == 'staging-gitlab'\
            or self.DEVELOPMENT_ENV == 'production-heroku'
        if self.DEVELOPMENT_ENV == 'staging-gitlab':
            chrome_options.add_argument('--dns-prefetch-disable')
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(E2ETest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(E2ETest, self).tearDown()

    def test_can_open_the_web(self):
        self.selenium.get('http://localhost:8000')
        time.sleep(2)
        assert 'apa kabar?' in self.selenium.page_source

    def test_add_entry(self):
        pattern = 'quick brown fox jumps over the lazy dog'
        self.selenium.get('http://localhost:8000')
        initialCount = self.selenium.page_source.count(pattern)
        status_input = self.selenium.find_element_by_id('status')
        status_submit = self.selenium.find_element_by_id('status_submit')
        time.sleep(2)
        status_input.send_keys(pattern)
        time.sleep(2)
        status_submit.submit()
        time.sleep(2)
        lastCount = self.selenium.page_source.count(pattern)
        assert lastCount - initialCount == 1

    def test_advance_to_other_fwpidnppwlab(self):
        self.selenium.get('http://localhost:8000')
        time.sleep(3)
        self.selenium.find_element_by_id('fwpidnppwlab_button').click()
        time.sleep(3)
        assert 'fwpidnppwlab.herokuapp.com' in self.selenium.current_url

    def test_profile_page(self):
        self.selenium.get('http://localhost:8000/profile/')
        time.sleep(2)
        assert 'Febriananda Wida Pramudita' in self.selenium.page_source

    def test_profile_page_if_have_gui(self):
        self.DEVELOPMENT_ENV == 'local' and self.selenium.get('http://localhost:8000')
        time.sleep(5)
        self.DEVELOPMENT_ENV == 'local' and self.selenium.find_element_by_id('nav_profile').click()
        time.sleep(2)
        self.DEVELOPMENT_ENV == 'local' and 'Febriananda Wida Pramudita' in self.selenium.page_source

    def test_book_that_haventread(self):
        self.selenium.get('http://localhost:8000/book/')
        time.sleep(5)
        assert 'Banyaknya buku favorit: ' in self.selenium.page_source


class FunctionalTest(TestCase):
    @classmethod
    def setUpClass(self):
        super().setUpClass()
        chrome_options = Options()
        DEVELOPMENT_ENV = os.environ.get('DEVELOPMENT_ENV')
        assert DEVELOPMENT_ENV == 'local' or DEVELOPMENT_ENV == 'staging-gitlab'\
            or DEVELOPMENT_ENV == 'production-heroku'
        if DEVELOPMENT_ENV == 'staging-gitlab':
            chrome_options.add_argument('--dns-prefetch-disable')
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000')

    @classmethod
    def tearDownClass(self):
        super().tearDownClass()
        self.selenium.quit()

    def test0A_check_loading_spinner(self):
        main_element = self.selenium.find_element_by_class_name('main_element')
        assert main_element.value_of_css_property('display') == 'none'
        time.sleep(5)
        assert main_element.value_of_css_property('display') != 'none'

    def test0B_check_greeting_message_in_page(self):
        assert 'apa kabar?' in self.selenium.page_source
        time.sleep(3)

    def test1_check_title(self):
        assert self.selenium.title == 'Website dedicated to Fata Nugraha'
        time.sleep(2)

    def test2_css_background_is_light(self):
        body_element = self.selenium.find_element_by_id('body_element')
        assert body_element.value_of_css_property('background-color') == 'rgba(255, 255, 255, 1)'

    def test3_css_background_is_dark(self):
        self.selenium.find_element_by_id('themeChanger').click()
        body_element = self.selenium.find_element_by_id('body_element')
        assert body_element.value_of_css_property('background-color') == 'rgba(0, 0, 0, 1)'

    def test4_add_status(self):
        pattern = 'quick brown fox jumps over the lazy dog'
        initialCount = self.selenium.page_source.count(pattern)
        status_input = self.selenium.find_element_by_id('status')
        status_submit = self.selenium.find_element_by_id('status_submit')
        time.sleep(2)
        status_input.send_keys(pattern)
        time.sleep(2)
        status_submit.submit()
        time.sleep(2)
        lastCount = self.selenium.page_source.count(pattern)
        assert lastCount - initialCount == 1

    def test5_move_to_profile(self):
        self.selenium.get('http://localhost:8000/profile')
        time.sleep(5)
        assert 'Febriananda Wida Pramudita' in self.selenium.page_source
        assert 'Batman' in self.selenium.page_source
        self.selenium.find_element_by_id('themeChanger').click()
        assert 'Febriananda Wida Pramudita' in self.selenium.page_source
        assert 'Fasilkom' in self.selenium.page_source


class OauthSurfaceTest(TestCase):
    def test_login(self):
        resp = self.client.get('/oauth/login/')
        self.assertEqual(resp.status_code, 200)

    def test_logout(self):
        resp = self.client.get('/oauth/logout_user/')
        self.assertEqual(resp.status_code, 302)

    def test_complete(self):
        resp = self.client.get('/oauth/complete/')
        self.assertEqual(resp.status_code, 302)
