from django.urls import re_path
from .views import index, profile, get_book_api, book, subscribe, check_email_api,\
    register_as_subscriber, count_subscriber, list_subscriber_api, subscribers, \
    unsubscribe
# url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^profile/$', profile, name='profile'),
    re_path(r'^book/$', book, name='book'),
    re_path(r'^get_book_api/$', get_book_api, name='get_book_api'),
    re_path(r'^subscribe/$', subscribe, name='subscribe'),
    re_path(r'^check_email_api/$', check_email_api, name='check_email_api'),
    re_path(r'^register_as_subscriber/$', register_as_subscriber, name='register_as_subscriber'),
    re_path(r'^count_subscriber/$', count_subscriber, name='count_subscriber'),
    re_path(r'^list_subscriber_api/$', list_subscriber_api, name='list_subscriber_api'),
    re_path(r'^subscribers/$', subscribers, name='subscribers'),
    re_path(r'^unsubscribe/$', unsubscribe, name='unsubscribe'),
    # re_path(r'^count_subscriber/$', count_subscriber, name='count_subscriber'),
]
