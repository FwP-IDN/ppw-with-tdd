from django.shortcuts import render
from lab_6.models import PersonalStatus, Subscriber
from django.http import HttpResponse
import datetime
import requests
import json


def index(request):
    if request.method == 'POST':
        now = datetime.datetime.now() + datetime.timedelta(hours=7)
        print(now)
        _status = request.POST.get('status')
        newData = PersonalStatus(created_at=now,
                                 status=_status,
                                 )
        newData.save()
    context = {}
    personalSchedules = PersonalStatus.objects.all()
    datas = [None] * len(personalSchedules)
    for i in range(len(datas)):
        rawItem = personalSchedules[i]
        _created_at = rawItem.created_at
        _status = rawItem.status

        datas[i] = {
            'created_at': _created_at,
            'status': _status,
        }
    context = {'datas': datas}
    return render(request, 'lab_6/index.html', context)


def profile(request):
    context = {}
    return render(request, 'lab_6/profile.html', context)


def book(request):
    context = {}
    request.session['fav_book_count'] = request.session.get('fav_book_count', 0)
    if(request.method == 'POST'):
        request.session['fav_book_count'] += int(request.POST['fav_changer'])
    return render(request, 'lab_6/book.html', context)


def get_book_api(request):
    json_book_collection = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + request.GET.get('q')).text
    return HttpResponse(json_book_collection, content_type="application/json")


def subscribe(request):
    context = {}
    return render(request, 'lab_6/subscribe.html', context)


def register_as_subscriber(request):
    if request.method == "POST" and len(Subscriber.objects.filter(email=request.POST["inputEmail1"])) == 0:
        newData = Subscriber(
            name=request.POST["inputName1"],
            email=request.POST["inputEmail1"],
            password=request.POST["inputPassword1"],

        )
        newData.save()
        return HttpResponse(json.dumps({"message": "REGISTRASI BERHASIL"}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({"message": "REGISTRASI GAGAL"}), content_type="application/json")


def check_email_api(request):
    email = request.GET.get("email")
    accounts = Subscriber.objects.filter(email=email)
    if len(accounts):
        return HttpResponse(json.dumps({"result": "REGISTERED"}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({"result": "UNREGISTERED"}), content_type="application/json")


def count_subscriber(request):
    return HttpResponse(json.dumps({"count": Subscriber.objects.count()}), content_type="application/json")


def list_subscriber_api(request):
    result = []
    subscribers = Subscriber.objects.all()
    for subscriber in subscribers:
        result.append({
            'name': subscriber.name,
            'email': subscriber.email,
            'id': subscriber.id,
        })
    return HttpResponse(json.dumps(result), content_type="application/json")


def subscribers(request):
    context = {}
    return render(request, 'lab_6/subscribers.html', context)


def unsubscribe(request):
    if request.method == 'POST':
        erasedSubscriber = Subscriber.objects.get(pk=int(request.POST.get('hiddenid')))
        if erasedSubscriber.password == request.POST['inputPasswordUnsubscribe']:
            erasedSubscriber.delete()
            return HttpResponse(json.dumps({"status": "SUCCESS"}), content_type="application/json")
        else:
            return HttpResponse(json.dumps({"status": "FAIL, UNMATCHED PASSWORD"}), content_type="application/json")
