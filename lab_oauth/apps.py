from django.apps import AppConfig


class LabOauthConfig(AppConfig):
    name = 'lab_oauth'
