from django.urls import re_path
from .views import login, complete, logout_user
# url for app
urlpatterns = [
    re_path(r'^login/$', login, name='login'),
    re_path(r'^complete/$', complete, name='complete'),
    re_path(r'^logout_user/$', logout_user, name='logout_user'),
]
