from django.shortcuts import render, redirect
from django.contrib.auth import logout

# Create your views here.


def login(request):
    return render(request, 'lab_oauth/login.html')


def complete(request):
    return redirect('/')


def logout_user(request):
    logout(request)
    return redirect('/')


def set_extra_data_on_session(backend, strategy, details, response,
                              user=None, *args, **kwargs):
    if backend.name == 'google-oauth2':
        strategy.session_set('id_token', response['id_token'])
        strategy.session_set('email', response['emails'][0].get('value'))
        strategy.session_set('display_name', response['displayName'])
        strategy.session_set('image_url', response['image'].get('url'))
