$(document).ready(function(){
    function sleep (time) {
        return new Promise((resolve) => setTimeout(resolve, time));
    }

    sleep(1000).then(() => {
        $('.main_element').css('display', 'block');
        $('.loader').css('display', 'none');
        $("#accordion").accordion();
        $("#accordion").click(function() {
            $(this).accordion();
            setTheme();
        });
        function setTheme() {
            if(localStorage.theme === "light") {
                $("body").css("background-color", "white");
                $("h1").css("color", "black");
                $(".navbar").css("background-color", "#007bff");
                $(".btn").css("background-color", "#007bff");
                $(".btn").css("color", "white");
                $("legend").css("color", "black");
                $("label").css("color", "black");
                $(".form-control").css("background-color", "white");
                $(".jumbotron").css("background-color", "#e9ecef");
                $(".headrow").css("background-color", "#007bff");
                $("td").css("color", "black");
                $(".table-secondary").children("td").css("background-color", "#b8daff");
                $(".table-primary").children("td").css("background-color", "#d6d8db");
                $("p").css("color", "black");
                $("img").attr("src", "https://media.licdn.com/dms/image/C5603AQFQAfVMchgMvw/profile-displayphoto-shrink_200_200/0?e=1544659200&v=beta&t=buKPdQjY0lvXuOo3O6IYyie6wzMP9OHZge67GkSNjYg");
                $("img").css("border-color", "#007bff");
                $(".ui-state-default").css("background-color", "#e9ecef");
                $(".ui-state-active").css("background-color", "#007bff");
                $(".ui-widget-content").css("background-color", "white");
                $(".ui-widget-content").css("color", "black");
                $("#introduction").text("Nama saya Febriananda Wida Pramudita, seorang mahasiswa Fasilkom UI angkatan 2017");
            }
            else {
                $("body").css("background-color", "black");
                $("h1").css("color", "white");
                $(".navbar").css("background-color", "#003b7f");
                $(".btn").css("background-color", "#003b7f");
                $(".btn").css("color", "white");
                $("legend").css("color", "white");
                $("label").css("color", "white");
                $(".form-control").css("background-color", "black");
                $(".jumbotron").css("background-color", "#161310");
                $(".headrow").css("background-color", "#003b7f");
                $("td").css("color", "white");
                $(".table-secondary").children("td").css("background-color", "#5c6d7f");
                $(".table-primary").children("td").css("background-color", "#6b6c6d");
                $("p").css("color", "white");
                $("img").attr("src", "https://i.imgur.com/nQ2vvcR.jpg");
                $("img").css("border-color", "#003b7f");
                $(".ui-state-default").css("background-color", "#6b6c6d");
                $(".ui-state-active").css("background-color", "#003b7f");
                $(".ui-widget-content").css("background-color", "black");
                $(".ui-widget-content").css("color", "white");
                $("#introduction").text("Nama saya Febriananda Wida Pramudita, saya bukan Batman");
            }
        }

        if (typeof(localStorage) === "undefined") {
            alert("No localStorage, install it or update to html5");
        }

        if(localStorage.theme !== "dark" && localStorage.theme !== "light") {
            localStorage.theme = "light";
        }

        setTheme();

        $("#themeChanger").click(function() {
            if(localStorage.theme === "dark") {
                localStorage.theme = "light";
            }
            else {
                localStorage.theme = "dark";
            }
            setTheme();
        });
    });
})